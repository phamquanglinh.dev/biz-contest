import './App.css';
import Countdown from "react-countdown";
import QuestionBody from "./components/QuestionBody";
import {useEffect, useState} from "react";
import axios from "axios";
import {toast, ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function App() {
    const baseApi = "https://bizenglish-edu.net/api"
    const baseUrl = "https://bizenglish-edu.net/"
    const [token, setToken] = useState('')
    const [userId, setUserId] = useState("")
    const [contest_id, setContestId] = useState('')
    useEffect(() => {
        const queryString = window.location.search
        const urlParams = new URLSearchParams(queryString);
        setToken(urlParams.get('token'))
        setContestId(urlParams.get("contest"))
        setUserId(urlParams.get("user_id"))
    }, [])
    const [start, setStart] = useState(false)
    const getContest = async () => {
        try {
            const response = await (await axios.post(baseApi + "/get-contest", {
                contest_id: parseInt(contest_id),
                user_id: userId,
            }, {
                headers: {
                    'Authorization': token
                }
            }))
            console.log(response.data.body)
            setLimit(response.data.limit)
            setTitle(response.data.title)
            setQuestions(response.data.body)
        } catch (e) {
            toast.error("Không tìm thấy", {autoClose: 200})
            window.location.href = baseUrl
        } finally {
            setFetching(false)
        }
    }
    const submitContest = async ({data}) => {
        console.log("submitData:", data)
        try {
            await (await axios.post(baseApi + "/submit-contest", {
                submitData: data,
                contest_id: contest_id,
                user_id: userId,
            }, {
                headers: {
                    'Authorization': token
                }
            }))
            toast.success("Nộp thành công")
            window.location.href = baseUrl
        } catch (e) {
            toast.error("Nộp bài lỗi, vui lòng thử lại")
        }
    }
    const [end, setEnd] = useState(false)
    const [questions, setQuestions] = useState([])
    const [limit, setLimit] = useState(60)
    const [title, setTitle] = useState("")
    const [fetching, setFetching] = useState(true)

    useEffect(() => {
        if (start) {
            getContest().then()
        }
    }, [start])
    return (
        <div className={"flex h-[90vh] w-full items-center justify-center"}>
            {start ?
                <div className={"w-full m-auto"}>
                    {!fetching ?
                        <div className={"m-auto container max-w-[100rem] p-5 bg-gray-50 border mt-5 rounded shadow-lg"}>
                            <div className={"text-2xl text-blue-900 text-center font-bold uppercase"}>{title}</div>
                            <div className={"flex flex-wrap justify-center items-center"}>
                                <div className={"text-blue-900"}>Thời gian làm bài :</div>
                                <Countdown
                                    className={"text-center text-blue-900 block"}
                                    date={Date.now() + limit * 60 * 1000}
                                    onComplete={() => {
                                        setEnd(true)
                                    }}
                                />
                            </div>
                            <QuestionBody end={end} questions={questions} submit={submitContest}/>
                            <ToastContainer/>
                        </div> : null}
                </div> :
                <div
                    onClick={() => {
                        setStart(true)
                    }}
                    className={"text-3xl bg-blue-900 text-white p-5 hover:bg-blue-600 transition-all rounded cursor-pointer"}>
                    Bắt đầu làm bài
                </div>
            }
        </div>
    );
}

export default App;
