import {useEffect, useState} from "react";
import Short from "./Short";
import {toast} from "react-toastify";
import Video from "./Video";
import Audio from "./Audio";
import axios from "axios";
import Pagination from "./Pagination";

const QuestionBody = ({end, questions, submit}) => {

    const [current, setCurrent] = useState({})
    const [data, setData] = useState([])
    const [textCorrecting, setTextCorrecting] = useState('')

    const answer = ({short, long}) => {
        let newAnswers = {
            ...current,
            user_choose: short,
            user_type: long,
            id: current.currentKey,
        }
        let hasItem = false
        const tmp = data.map((v) => {
            if (v.id === newAnswers.id) {
                hasItem = true
                return {...v, newAnswers}
            }
            return v
        })
        setData(tmp)
        if (!hasItem) {
            setData([...data, newAnswers])
        }
        if (newAnswers.id >= questions.length - 1) {
            toast.success("Hết câu hỏi, có thể nộp bài")
        } else {
            const nextQuestionId = current.currentKey + 1
            setCurrent({...questions[nextQuestionId], currentKey: nextQuestionId})
        }
    }
    useEffect(() => {
        if (end === true)
            submit({data: data})
    }, [end])
    useEffect(() => {
        setCurrent({...questions[0], currentKey: 0})
    }, [questions])
    return (
        <div className={"max-w-[75%] m-auto"}>
            <Pagination
                setCurrent={setCurrent}
                data={data}
                questions={questions}
                current={current}
                submit={submit}
            />
            <div
                className={"shadow-lg mt-5 p-5 rounded-3xl py-10 text-white font-semibold text-center text-3xl border bg-blue-900"}>
                <span>
                    Question {current.currentKey + 1}. {current.question}
                </span>
                {current.image !== "" ?
                    <div className={"text-center"}>
                        <img className={"w-1/2 rounded m-auto mt-5"}
                             src={"https://bizenglish-edu.net/" + current.image} alt={""}/>
                    </div>
                    : null}
                {current.audio !== "" && current.audio !== undefined ?
                    <div className={"text-center"}>
                        <Audio link={"https://bizenglish-edu.net/" + current.audio} delay={current.delay}/>
                    </div>
                    : null}
                {current.video !== "" && current.video !== undefined ?
                    <div className={"text-center mt-5"}>
                        <div className={"m-auto"}>
                            <Video link={current.video}/>
                        </div>
                    </div>
                    : null}
            </div>

            {current.text_correct === ""
                ?
                <div className={"flex flex-wrap mt-5"}>
                    <div className={"md:basis-1/2 basis-full pr-2 mb-3"}>
                        <Short
                            answer={answer}
                            label={current.a}
                            value={'a'}
                        />
                    </div>
                    <div className={"md:basis-1/2 basis-full pr-2 mb-3"}>
                        <Short
                            answer={answer}
                            label={current.b}
                            value={'b'}
                        />
                    </div>
                    <div className={"md:basis-1/2 basis-full pr-2 mb-3"}>
                        <Short
                            answer={answer}
                            label={current.c}
                            value={'c'}
                        />
                    </div>
                    <div className={"md:basis-1/2 basis-full pr-2 mb-3"}>
                        <Short
                            answer={answer}
                            label={current.d}
                            value={'d'}
                        />
                    </div>
                </div>
                :
                <div>
                    <input
                        onChange={(r) => {
                            setTextCorrecting(r.target.value)
                        }}
                        placeholder={"Nhập đáp án"}
                        className={"shadow-lg text-center p-5 text-2xl mt-5 w-ful border-blue-600  w-full"}/>
                    <div
                        onClick={() => {
                            answer({long: textCorrecting})
                            setTextCorrecting('')
                        }}
                        className={"shadow-lg cursor-pointer uppercase text-2xl mt-5 text-center bg-blue-900 p-5 rounded-3xl hover:font-bold transition-all text-white"}>Trả
                        lời
                    </div>
                </div>
            }
        </div>
    )
}
export default QuestionBody