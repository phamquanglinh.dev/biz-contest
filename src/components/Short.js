const Short = ({answer, value, label}) => {
    return (
        <div
            className={"hover:text-white hover:bg-blue-900 border-blue-900 border-2 text-blue-900 shadow-lg  p-5 py-10 rounded-3xl cursor-pointer  transition-all text-center text-2xl"}
            onClick={() => {
                answer({short: value})
            }}>{value.toUpperCase()}. {label}
        </div>
    )
}
export default Short