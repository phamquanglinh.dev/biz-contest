const Video = ({link}) => {

    const url = new URL(link ?? "https://a.b");
    const embedId = url.searchParams.get("v")
    const embedLink = "https://youtube.com/embed/" + embedId + "?autoplay=1"
    return (
        <iframe style={{aspectRatio: 16 / 9}} className={"w-1/2 m-auto rounded"} allow={"autoplay"}
                src={embedLink}></iframe>
    )
}
export default Video