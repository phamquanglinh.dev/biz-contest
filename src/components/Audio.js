import {useEffect, useRef} from "react";

const Audio = ({link, delay}) => {
    const audioRef = useRef()
    useEffect(() => {
        if (link !== "") {
            audioRef.current.load()
            if (audioRef.current.currentTime > 0) {
                audioRef.current.pause()
            }
            setTimeout(() => {
               try {
                   audioRef.current.play()
               }catch (e) {
                   
               }
            }, delay * 1000)
        }
    }, [link])
    return (
        <audio ref={audioRef} controls={true}
               autoPlay={false}
               className={"w-1/2 rounded m-auto mt-5"}>
            <source src={link}/>
        </audio>
    )
}
export default Audio