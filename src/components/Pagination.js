const Pagination = ({questions, data, setCurrent, current, submit}) => {
    const changeQuestion = (key) => {
        setCurrent({...questions[key], currentKey: key})
    }
    return (
        <div className={"flex flex-row items-center justify-between"}>
            <div className={"flex items-center"}>
                <span className={"mr-2 font-bold"}>Câu hỏi :</span>
                {questions.map((item, key) =>
                    <div
                        onClick={() => {
                            changeQuestion(key)
                        }}
                        className={(data.find(v => v.id === key) ? "bg-green-500 text-white" : "") + " border shadow-lg cursor-pointer p-2  flex justify-center items-center mx-1 rounded-full w-[2rem] h-[2rem] " + (current.currentKey === key ? "bg-blue-900 text-white" : "text-blue-900")}
                        key={key}>{key + 1}
                    </div>
                )}
            </div>
            <span
                onClick={() => {
                    submit({data: data})
                }}
                className={"cursor-pointer hover:bg-green-950 transition-all bg-green-600 p-2 rounded text-white "}>Nộp bài</span>
        </div>
    )
}
export default Pagination